PYTHON ?= python3
PACKAGE=$(shell echo *.spec.in | sed -e 's/.spec.in$$//')

sources: ;

# -- source files

lalsuite.spec: render.py setup.cfg lalsuite.spec.in
	@echo -- Generating $@
	$(PYTHON) render.py

debian/control: render.py setup.cfg debian/control.in
	@echo -- Generating $@
	$(PYTHON) render.py

# -- source distribution

lalsuite-%.tar.gz: lalsuite.spec debian/changelog debian/compat debian/control debian/copyright debian/*.install debian/rules debian/source/format
	@echo -- Generating source distribution $@
	$(PYTHON) setup.py --quiet sdist --dist-dir .

.PHONY: dist
dist: lalsuite-*.tar.gz

# -- RHEL

lalsuite-%.src.rpm: lalsuite-*.tar.gz
	@echo -- Generating source RPM $@
	rpmbuild -D '_sourcedir ./' -D '_srcrpmdir ./' -ts $<

lalsuite-%.rpm: lalsuite-*.src.rpm
	@echo -- Generating binary RPM $@
	rpmbuild -D '_sourcedir ./' -D '_srcrpmdir ./' -D '_rpmdir ./' --rebuild $<

.PHONY: srpm
srpm: lalsuite-*.src.rpm

# -- Debian

lalsuite_%.orig.tar.gz: lalsuite-*.tar.gz
	@cp $< `basename $< | sed 's|\(.*\)-\(.*\).\(tar\..*\)|\1_\2.orig.\3|'`

lalsuite_%.dsc: lalsuite_*.orig.tar.gz
	@echo -- Generating debian source package $@
	@mkdir _dsc_src
	@tar -xf $< -C _dsc_src --strip-components=1
	dpkg-source --build _dsc_src || { rm -rf _dsc_src; exit 1; }
	@rm -rf _dsc_src

.PHONY: dsc
dsc: lalsuite_*.dsc

lalsuite_%.deb: lalsuite_*.dsc lalsuite_*.orig.tar.gz
	@echo -- Generating debian binary package $@
	@rm -rf _deb_src
	dpkg-source --extract $< _deb_src
	cd _deb_src; \
	mk-build-deps \
		--tool "apt-get -y -q -o Debug::pkgProblemResolver=yes --no-install-recommends" \
		--install \
		--remove \
	|| { rm -rf _deb_src; exit 1; }; \
	dpkg-buildpackage -us -uc -b || { rm -rf _deb_src; exit 1; }
	@rm -rf _deb_src

.PHONY: deb
deb: lalsuite_*.deb

.PHONY: clean
clean:
	rm -rvf \
		./debian/control \
		./lalsuite.spec \
		./lalsuite*.deb \
		./lalsuite_*.buildinfo \
		./lalsuite_*.changes \
		./lalsuite_*.debian.tar.* \
		./lalsuite_*.dsc \
		./lalsuite_*.orig.tar.* \
		./lalsuite-*.rpm \
		./lalsuite-*.tar.* \
	;
