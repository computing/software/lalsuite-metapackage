#!/usr/bin/env python3

"""Render the LALSuite metapackage files
"""

import sys
from configparser import ConfigParser
from pathlib import Path
from string import Template

import jinja2


def load_config_file(path):
    conf = ConfigParser()
    with open(path, "r") as file:
        conf.read_file(file)
    return conf


def render_template(path, mapping):
    return jinja2.Template(path.read_text()).render(**mapping)


def main():
    conf = load_config_file("setup.cfg")

    for path in map(Path, (
        "lalsuite.spec.in",
        "debian/control.in",
    )):
        rendered = render_template(path, conf)
        path.with_suffix("").write_text(rendered)


if __name__ == "__main__":
    sys.exit(main())
